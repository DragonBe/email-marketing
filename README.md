# Email Campaign

This is a small project to try out several approaches using tables and
embedded images for generating email campaigns.

## Requirement

For this tool to run, use [SwiftMailer](https://github.com/swiftmailer/swiftmailer) 
which is a [Composer](https://getcomposer.org) package. But you can use other
email libraries as well.

## Installation

```
composer install
```

## Usage

In [generate-email.php](generate-email.php) you need to add your various
email addresses to test design. I've tested against major online and offline
email applications, like:

- [Google Mail](https://mail.google.com)
- [Yahoo! Mail](https://mail.yahoo.com)
- [Outlook.com](https://outlook.com)
- [GMX.com](https://gmx.com)
- Apple Mail
- Thunderbird
- Pine
- Outlook

## License and terms of use

This software is provided "as-is" and released under [MIT license](LICENSE).
You're free to use, modify and redistribute this software but at your own risk.