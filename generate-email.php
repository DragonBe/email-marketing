<?php

/**
 * File: generate-email.php
 *
 *
 */
require_once 'vendor/autoload.php';

$transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 465, 'ssl')
    ->setUsername('your-username')
    ->setPassword('your-password');
$mailer = Swift_Mailer::newInstance($transport);

$images = array (
    'header' => 'welcome_header.png',
    'activate' => 'activate_your_account_button.png',
    'details' => 'view_details.png'
);

$message = Swift_Message::newInstance();

$txtBody = file_get_contents('email-campaign/welcome-template.txt');
$htmlBody = file_get_contents('email-campaign/welcome-template.html');

foreach ($images as $placeholder => $image) {
    $cid = $message->embed(Swift_Image::fromPath('email-campaign/' . $image)->setDisposition('inline'));
    $htmlBody = str_replace('{{' . $placeholder . '}}', $cid, $htmlBody);
}

$message->setSubject('Welcome to the in2it family')
    ->setFrom(array ('news@in2it.be' => 'In2it Newsletter'))
    ->addTo('news@in2it.be', 'In2it Newsletter') // Outlook client
    ->addBcc('info@in2it.be')                    // Gmail client
    ->setBody($htmlBody, 'text/html')
    ->addPart($txtBody, 'text/plain');


$count = $mailer->send($message);
echo 'Sending out ' . $count . ' message(s)' . PHP_EOL;